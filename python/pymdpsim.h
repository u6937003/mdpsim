#ifndef _PYMDPSIM_H
#define _PYMDPSIM_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <cassert>
#include <vector>
#include <map>
#include <memory>
#include <regex>
#include <sstream>
#include <utility>
#include <type_traits>

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"

#include "states.h"
#include "problems.h"
#include "domains.h"

using std::string;
using std::vector;
using std::map;
using std::pair;
using std::unique_ptr;
using std::make_unique;
using std::shared_ptr;
using std::make_shared;
using std::make_pair;
using std::stringstream;
using std::regex;
using std::smatch;
using std::sregex_iterator;
namespace py = pybind11;

typedef std::shared_ptr<State> StatePtr;

class PyDomain;
class PyProblem;

void build_maps(
  const Problem *,
  AtomList &,
  FluentList &,
  vector<pair<const Comparison*, const Comparison*>> &,
  vector<const Action*> &);

class PyTerm {
 public:
  explicit PyTerm(const Term term, const Domain *domain);
  virtual ~PyTerm() {};
  bool is_variable() const {return term.variable();}
  bool is_object() const {return term.object();}
  long hash() const;
  bool eq(PyTerm *other) const;
  virtual const string repr() const = 0;
  const string type() const;
  const string name() const;

 private:
  const Term term;
  const Domain *domain;
  const int index;
};

class PyPDDLObject : public PyTerm {
public:
  PyPDDLObject(const Object object, const Domain *domain);
  const string repr() const override;

private:
  const Object object;
};

class PyVariable : public PyTerm {
 public:
  explicit PyVariable(const Variable variable, const Domain *domain);
  const string repr() const override;

 private:
  const Variable variable;
};

class PyPredicate {
public:
  PyPredicate(const Domain *domain, const string name);
  const string name() const;
  const string repr() const;
  const py::list arg_types();

private:
  const Domain *domain;
  const string name_;
};

class PyProposition {
public:
  PyProposition(const Atom *atom, const Domain *domain, bool is_goal_);
  PyProposition(const Atom *atom, const Domain *domain);
  PyPredicate *predicate();
  bool in_goal();
  py::list terms();
  const string identifier();
  const string repr();

private:
  const Atom *atom;
  const Domain *domain;
  bool is_goal_;
  bool has_goal_info_;
};

class PyFunction {
public:
  PyFunction(const Domain *domain, const string name);
  const string name() const;
  const string repr() const;
  const py::list arg_types() const;
  bool is_static() const;

private:
  const Domain *domain;
  const string name_;
};

class PyFluent {
public:
  PyFluent(const Fluent *fluent, const Domain *domain, bool is_goal_);
  PyFluent(const Fluent *fluent, const Domain *domain);
  PyFunction *function();
  bool is_static() const;
  bool in_goal();
  py::list terms();
  const string identifier();
  const string repr();

private:
  const Fluent *fluent;
  const Domain *domain;
  bool is_goal_;
  bool has_goal_info_;

  friend PyProblem;
};

class PyLiftedAction {
public:
  PyLiftedAction(const ActionSchema *schema, const Domain *domain);
  const string name() const;
  const string repr() const;
  const py::list parameters_and_types() const;
  // all effects and precondition propositions, including duplicates
  const py::list involved_propositions() const;
  // all functions related to this action
  const py::list involved_functions() const;
  // all lifted comparisons related to this function
  const py::list involved_lifted_comparisons() const;

private:
  const ActionSchema *schema;
  const Domain *domain;
};

class PyGroundAction {
public:
  PyGroundAction(const Action *action, const Problem *problem);
  const PyLiftedAction lifted_action() const;
  const py::list arguments() const;
  const py::list involved_propositions() const;
  const py::list involved_fluents() const;
  const py::list involved_comparisons() const;
  const string identifier() const;
  const string repr() const;

private:
  const Action *action;
  const Problem *problem;
  const ActionSchema *get_schema() const;
  friend PyProblem;
};

class PyLiftedComparison {
public:
  PyLiftedComparison(const Comparison *schema, const Domain *domain);
  const string str() const;
  const string repr() const;
  const py::list parameters_and_types() const;

private:
  const Comparison *schema;
  const Domain *domain;
};

class PyGroundComparison {
public:
  PyGroundComparison(const Comparison *comparison,
                     const Comparison* lifted,
                     const Problem *problem);
  const PyLiftedComparison lifted_comparison() const;
  const string str() const;
  const string repr() const;

private:
  const Comparison *comparison;
  const Comparison *lifted;
  const Problem *problem;
};

class PyProblem {
public:
  PyProblem(const PyProblem &other);
  PyProblem(const Problem *problem);
  const string name() const;
  const string repr() const;
  const bool is_simple_numeric() const;
  const py::list propositions() const;
  const py::list ground_actions() const;
  const py::list fluents() const;
  const py::list comparisons() const;
  bool is_goal_atom(const Atom *at) const;
  bool is_goal_fluent(const Fluent *fl) const;
  const PyDomain domain() const;
  // new stuff:
  py::list prop_truth_mask(const State &state) const;
  py::list fluent_value_mask(const State &state) const;
  py::list comp_truth_mask(const State &state) const;
  py::list act_applicable_mask(const State &state) const;
  PyProblem problem_from_state(const State &state) const;
  double static_fluent_value(const PyFluent &fluent) const;
  StatePtr init_state() const;
  // constructs an intermediate state, caring ONLY about atoms
  StatePtr intermediate_atom_state(const string &props_true) const;
  // constructs an intermediate state, caring about both atoms and fluents
  StatePtr intermediate_state(const string &props_true, 
                              const string &fluent_values) const;
  StatePtr apply(StatePtr state, const PyGroundAction &action) const;
  bool applicable(StatePtr state, const PyGroundAction &action) const;
  size_t num_actions() const;
  size_t num_props() const;
  size_t num_fluents() const;
  size_t num_comparisons() const;
  // Note that you can use problem->goal().progress(terms, atoms, values) to get
  // FPG-style progress measures. Not implemented now for lack of need.

private:
  const Problem *problem;
  AtomSet goal_atoms;
  AtomList atom_vec;
  FluentSet goal_fluents;
  FluentList fluent_vec;
  // List of comparisons and their lifted counterpart
  vector<pair<const Comparison*, const Comparison*>> comparison_vec;
  vector<const Action*> action_vec;

  // "modified ES262 syntax" (makes it easy to test =D)
  const regex prop_re = regex("\\s*((?:[^,\\s]+\\s*)+)(?:,|$)");
  const regex tok_re = regex("[^\\s]+");
  const regex fluent_re = regex("\\s*((?:[^,:\\s]+\\s*)+):\\s*([+-]?(?:[0-9]+(?:[.][0-9]*)?|[.][0-9]+))(?:,|$)");

  void init_maps();
  AtomSet parse_true_atoms(const string &props_true) const;
  ValueMap parse_fluent_values(const string &fluents_values) const;
};

class PyDomain {
 public:
  PyDomain(const PyDomain &other);
  PyDomain(const Domain *domain);
  const string name() const;
  const string repr() const;
  py::list types();
  py::list predicates();
  py::list functions();
  py::list lifted_actions();
  py::list lifted_comparisons();
 private:
  const Domain *domain;
};

#endif // _PYMDPSIM_H
