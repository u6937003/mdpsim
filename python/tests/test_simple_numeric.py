# WARNING: Ugly ugly ugly thing about pytest: the global state of the mdpsim
# module is kept between different tests, so this test has to run after the
# test_numeric.py file, else it will somehow mess with the plant watering domain
# and cause one of thoses tests to fail. To do so we have to make sure that this
# file is alphabetically after the other one, which is why its name probably
# isn't very good.

import pytest
import mdpsim
import os
import re

parsed = False

lowerbound_regex = r"LiftedComparison\(<\(>= \(value \?(.*)\) 1\)>\)"
upperbound_regex = (
    r"LiftedComparison\(<\(<= \(\+ \(value \?(.*)\) 1\) \(max_int\)\)>\)"
)

def parse():
    global parsed
    if not parsed:
        my_path = os.path.dirname(os.path.abspath(__file__))
        counter_domain_path = os.path.join(
            my_path, "..", "..", "examples", "counter-domain.pddl"
        )
        counter_problem_path = os.path.join(
            my_path, "..", "..", "examples", "counter-problem-simple-numeric.pddl"
        )
        counter_bad_problem_path = os.path.join(
            my_path, "..", "..", "examples", "counter-problem-non-simple-numeric.pddl"
        )

        mdpsim.parse_file(counter_domain_path)
        mdpsim.parse_file(counter_problem_path)
        mdpsim.parse_file(counter_bad_problem_path)
        parsed = True


@pytest.fixture()
def counter_domain():
    parse()
    return mdpsim.get_domains()["fn-counters"]


@pytest.fixture()
def counter_problem():
    parse()
    return mdpsim.get_problems()["instance_6"]


@pytest.fixture()
def counter_bad_problem():
    parse()
    return mdpsim.get_problems()["instance_6_bad"]


def test_counter_problem_is_simple_numeric(counter_problem):
    assert counter_problem.is_simple_numeric


def test_counter_bad_problem_is_not_simple_numeric(counter_bad_problem):
    assert not counter_bad_problem.is_simple_numeric


def test_counter_domain_lifted_comparisons_basic(counter_domain):
    lifted_comparisons = counter_domain.lifted_comparisons
    assert len(lifted_comparisons) == 2


def test_counter_domain_lifted_comparisons_repr(counter_domain):
    lifted_comparisons = counter_domain.lifted_comparisons
    assert re.match(lowerbound_regex, repr(lifted_comparisons[0])) or re.match(
        upperbound_regex, repr(lifted_comparisons[0])
    )
    assert re.match(lowerbound_regex, repr(lifted_comparisons[1])) or re.match(
        upperbound_regex, repr(lifted_comparisons[1])
    )


def test_counter_domain_lifted_comparisons_params(counter_domain):
    lifted_comparisons = counter_domain.lifted_comparisons
    assert len(lifted_comparisons[0].parameters_and_types) == 1
    assert len(lifted_comparisons[1].parameters_and_types) == 1
    assert lifted_comparisons[0].parameters_and_types[0][1] == "counter"
    assert lifted_comparisons[1].parameters_and_types[0][1] == "counter"


def test_counter_domain_lifted_actions_basic(counter_domain):
    lifted_actions = counter_domain.lifted_actions
    assert len(lifted_actions) == 2
    assert {l.name for l in lifted_actions} == {"increment", "decrement"}


def test_counter_domain_lifted_actions_and_conditions_params(counter_domain):
    lifted_comparisons = counter_domain.lifted_comparisons
    lifted_actions = counter_domain.lifted_actions
    assert (
        lifted_comparisons[0].parameters_and_types
        == lifted_actions[0].parameters_and_types
        or lifted_comparisons[0].parameters_and_types
        == lifted_actions[1].parameters_and_types
    )
    assert (
        lifted_comparisons[1].parameters_and_types
        == lifted_actions[0].parameters_and_types
        or lifted_comparisons[1].parameters_and_types
        == lifted_actions[1].parameters_and_types
    )


def test_counter_domain_lifted_actions_involved_comparisons(counter_domain):
    lifted_comparisons = counter_domain.lifted_comparisons
    lifted_actions = counter_domain.lifted_actions

    for lifted_action in lifted_actions:
        for c in lifted_action.involved_lifted_comparisons:
            # direct comparison doesn't work as they are different objects
            assert repr(c) in {repr(comp) for comp in lifted_comparisons}

def test_counter_problem_comparisons_basic(counter_problem):
    comparisons = counter_problem.comparisons
    assert len(comparisons) == 12
    assert {repr(c) for c in comparisons} == {
        "GroundComparison(<(>= (value c0) 1)>)",
        "GroundComparison(<(>= (value c1) 1)>)",
        "GroundComparison(<(>= (value c2) 1)>)",
        "GroundComparison(<(>= (value c3) 1)>)",
        "GroundComparison(<(>= (value c4) 1)>)",
        "GroundComparison(<(>= (value c5) 1)>)",
        "GroundComparison(<(<= (+ (value c0) 1) 8)>)",
        "GroundComparison(<(<= (+ (value c1) 1) 8)>)",
        "GroundComparison(<(<= (+ (value c2) 1) 8)>)",
        "GroundComparison(<(<= (+ (value c3) 1) 8)>)",
        "GroundComparison(<(<= (+ (value c4) 1) 8)>)",
        "GroundComparison(<(<= (+ (value c5) 1) 8)>)",
    }

def test_counter_problem_comparisons_lifted_counterpart(counter_problem):
    comparisons = counter_problem.comparisons

    for c in comparisons:
        if '>=' in repr(c):
            assert re.match(lowerbound_regex, repr(c.lifted_comparison))
        else:
            assert re.match(upperbound_regex, repr(c.lifted_comparison))
    
def test_counter_problem_copmarison_satisfaction(counter_problem):
    comparisons = counter_problem.comparisons
    state = counter_problem.init_state()
    
    assert {repr(c): truth for c, truth 
            in counter_problem.comp_truth_mask(state)} == {
                "GroundComparison(<(>= (value c0) 1)>)": False,
                "GroundComparison(<(>= (value c1) 1)>)": False,
                "GroundComparison(<(>= (value c2) 1)>)": False,
                "GroundComparison(<(>= (value c3) 1)>)": False,
                "GroundComparison(<(>= (value c4) 1)>)": False,
                "GroundComparison(<(>= (value c5) 1)>)": False,
                "GroundComparison(<(<= (+ (value c0) 1) 8)>)": True,
                "GroundComparison(<(<= (+ (value c1) 1) 8)>)": True,
                "GroundComparison(<(<= (+ (value c2) 1) 8)>)": True,
                "GroundComparison(<(<= (+ (value c3) 1) 8)>)": True,
                "GroundComparison(<(<= (+ (value c4) 1) 8)>)": True,
                "GroundComparison(<(<= (+ (value c5) 1) 8)>)": True,
            }
