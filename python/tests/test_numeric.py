import pytest
import mdpsim
import os

parsed = False


def parse():
    global parsed
    if not parsed:
        my_path = os.path.dirname(os.path.abspath(__file__))
        pw_domain_path = os.path.join(
            my_path, '..', '..', 'examples', 'plant-watering-domain.pddl')
        pw_problem932_path = os.path.join(
            my_path, '..', '..', 'examples', 'plant-watering-problem-9-3-2.pddl')
        pw_problem411_path = os.path.join(
            my_path, '..', '..', 'examples', 'plant-watering-problem-4-1-1.pddl')
        mdpsim.parse_file(pw_domain_path)
        mdpsim.parse_file(pw_problem932_path)
        mdpsim.parse_file(pw_problem411_path)
        parsed = True


@pytest.fixture()
def pw_domain():
    parse()
    return mdpsim.get_domains()['mt-plant-watering-constrained']


@pytest.fixture()
def pw_problem932():
    parse()
    return mdpsim.get_problems()['instance_9_3_2']

@pytest.fixture()
def pw_problem411():
    parse()
    return mdpsim.get_problems()['instance_4_1_1']


def test_plant_watering_domain_repr(pw_domain):
    assert repr(pw_domain) == 'Domain(<mt-plant-watering-constrained>)'


def test_plant_watering_domain_types(pw_domain):
    types = pw_domain.types
    assert set(types) == {'thing', 'location', 'plant', 'tap', 'agent'}


def test_plant_watering_domain_predicates(pw_domain):
    preds = pw_domain.predicates
    assert len(preds) == 0


def test_plant_watering_domain_lifted_action(pw_domain):
    lifted_actions = pw_domain.lifted_actions
    lifted_names_expt = {
        'move_up', 'move_down', 'move_right', 'move_left', 'move_up_left',
        'move_up_right', 'move_down_left', 'move_down_right', 'load', 'pour'}

    assert len(lifted_actions) == len(lifted_names_expt)

    lifted_dict = {l.name: l for l in lifted_actions}
    assert lifted_dict.keys() == lifted_names_expt

    move_up = lifted_dict['move_up']
    params_types = move_up.parameters_and_types
    assert len(params_types) == 1
    assert params_types[0][1] == 'agent'
    assert repr(params_types[0][0]).startswith('Variable(')

    # Check involved propositions
    assert len(move_up.involved_propositions) == 0

    # Check involved functions
    assert {f.function.name for f in move_up.involved_functions} \
        == {'maxy', 'y'}
    
    # make sure maxy is static
    for f in move_up.involved_functions:
        if f.function.name == 'maxy':
            assert f.function.is_static


def test_plant_watering_problem_names(pw_problem932):
    assert repr(pw_problem932) == 'Problem(<instance_9_3_2>)'
    assert pw_problem932.name == 'instance_9_3_2'
    assert pw_problem932.domain.name == 'mt-plant-watering-constrained'


def test_plant_watering_problem_propositions(pw_problem932):
    props = pw_problem932.propositions
    assert len(props) == 0


def test_plant_watering_problem_ground_actions(pw_problem932):
    ground_actions = pw_problem932.ground_actions
    assert len(ground_actions) == 12
    assert {a.identifier for a in ground_actions} == {
        '(move_up agent1)', '(move_down agent1)', '(move_right agent1)',
        '(move_left agent1)', '(move_up_left agent1)', '(move_up_right agent1)',
        '(move_down_left agent1)', '(move_down_right agent1)',
        '(load agent1 tap1)', '(pour agent1 plant1)', '(pour agent1 plant2)',
        '(pour agent1 plant3)'}


def test_plant_watering_problem_fluents(pw_problem932):
    fluents = pw_problem932.fluents
    assert len(fluents) == 23
    assert {f.identifier for f in fluents} == {
        '(max_int)', '(maxx)', '(maxy)', '(miny)', '(minx)', '(x agent1)',
        '(y agent1)', '(x tap1)', '(y tap1)', '(x plant1)', '(y plant1)',
        '(x plant2)', '(y plant2)', '(x plant3)', '(y plant3)', '(carrying)',
        '(poured plant1)', '(poured plant2)', '(poured plant3)',
        '(total_poured)', '(total_loaded)', '(total-time)', '(goal-achieved)'}
    
    # check the static fluents
    static_fluents_expected = {
        '(max_int)': 60,
        '(maxx)': 9,
        '(maxy)': 9,
        '(miny)': 1,
        '(minx)': 1,
    }
    static_fluents = [f for f in fluents if f.is_static]
    assert {f.identifier for f in static_fluents} \
        == static_fluents_expected.keys()
        
    # check the values of the static fluents
    for static_fluent in static_fluents:
        assert static_fluents_expected[static_fluent.identifier] == \
            pw_problem932.static_fluent_value(static_fluent)



def test_plant_watering_goal_fluents(pw_problem932):
    goal_fluents = [f for f in pw_problem932.fluents if f.in_goal]
    assert {f.identifier for f in goal_fluents} == {
        '(poured plant1)', '(poured plant2)', '(poured plant3)',
        '(total_poured)'}


def test_plant_watering_problem_involved_propositions(pw_problem932):
    for a in pw_problem932.ground_actions:
        assert len(a.involved_propositions) == 0


def test_plant_watering_problem_involved_fluents(pw_problem932):
    action = None
    for ground_act in pw_problem932.ground_actions:
        if ground_act.identifier == '(load agent1 tap1)':
            action = ground_act
            break

    assert {f.identifier for f in action.involved_fluents} == {
        '(x agent1)', '(y agent1)', '(x tap1)', '(y tap1)', '(carrying)',
        '(total_loaded)'}


def test_plant_watering_init_state(pw_problem932):
    state = pw_problem932.init_state()

    assert pw_problem932.prop_truth_mask(state) == []
    assert {f.identifier: value
            for f, value in pw_problem932.fluent_value_mask(state)} == {
        '(max_int)': 60, '(maxy)': 9, '(miny)': 1, '(maxx)': 9, '(minx)': 1,
        '(carrying)': 0, '(total_poured)': 0, '(total_loaded)': 0,
        '(x agent1)': 9, '(y agent1)': 3, '(x plant2)': 6, '(y plant2)': 6,
        '(x plant3)': 3, '(y plant3)': 3, '(x plant1)': 9, '(y plant1)': 9,
        '(poured plant1)': 0, '(poured plant2)': 0, '(poured plant3)': 0,
        '(x tap1)': 7, '(y tap1)': 7, '(goal-achieved)': 0, '(total-time)': 0}


def test_plant_watering_apply_action(pw_problem932):
    state = pw_problem932.init_state()

    # Test the applicable actions first
    applicable_actions = [a
                          for a, applicable
                          in pw_problem932.act_applicable_mask(state)
                          if applicable]
    assert {a.identifier for a in applicable_actions} == {
        '(move_up agent1)', '(move_down agent1)', '(move_left agent1)',
        '(move_down_left agent1)', '(move_up_left agent1)'}

    for action in pw_problem932.ground_actions:
        assert pw_problem932.applicable(state, action) == \
            (action.identifier in {a.identifier for a in applicable_actions})

    # Apply an action
    move_up = list(filter(
        lambda a: a.identifier == '(move_up agent1)', applicable_actions))[0]
    new_state = pw_problem932.apply(state, move_up)

    # y agent1 should be 4 and time increases
    assert pw_problem932.prop_truth_mask(state) == []
    assert {f.identifier: value
            for f, value in pw_problem932.fluent_value_mask(new_state)} == {
        '(max_int)': 60, '(maxy)': 9, '(miny)': 1, '(maxx)': 9, '(minx)': 1,
        '(carrying)': 0, '(total_poured)': 0, '(total_loaded)': 0,
        '(x agent1)': 9, '(y agent1)': 4, '(x plant2)': 6, '(y plant2)': 6,
        '(x plant3)': 3, '(y plant3)': 3, '(x plant1)': 9, '(y plant1)': 9,
        '(poured plant1)': 0, '(poured plant2)': 0, '(poured plant3)': 0,
        '(x tap1)': 7, '(y tap1)': 7, '(goal-achieved)': 0, '(total-time)': 1}


def test_plant_watering_intermediate_state1(pw_problem932):
    fluent_dict = {
        '(max_int)': 60, '(maxy)': 9, '(miny)': 1, '(maxx)': 9, '(minx)': 1,
        '(carrying)': 0, '(total_poured)': 0, '(total_loaded)': 0,
        '(x agent1)': 9, '(y agent1)': 4, '(x plant2)': 6, '(y plant2)': 6,
        '(x plant3)': 3, '(y plant3)': 3, '(x plant1)': 9, '(y plant1)': 9,
        '(poured plant1)': 0, '(poured plant2)': 0, '(poured plant3)': 0,
        '(x tap1)': 7, '(y tap1)': 7, '(goal-achieved)': 0, '(total-time)': 1}

    state = pw_problem932.intermediate_state(
        '',
        ', '.join([
            '{}: {}'.format(k.strip('()'), v)
            for k, v in fluent_dict.items()])
    )

    assert pw_problem932.prop_truth_mask(state) == []
    assert {f.identifier: value
            for f, value in pw_problem932.fluent_value_mask(state)} == fluent_dict

def test_plant_watering_intermediate_state2(pw_problem411):
    fluent_str = 'carrying: 0.0, goal-achieved: 0.0, max_int: 20.0, maxx: 4.0, maxy: 4.0, minx: 1.0, miny: 1.0, poured plant1: 0.0, total-time: 0.0, total_loaded: 0.0, total_poured: 0.0, x agent1: 1.0, x plant1: 2.0, x tap1: 4.0, y agent1: 3.0, y plant1: 2.0, y tap1: 4.0'
    
    state = pw_problem411.intermediate_state(
        '',
        fluent_str)
    
    assert pw_problem411.prop_truth_mask(state) == []
    assert {f.identifier: value
            for f, value in pw_problem411.fluent_value_mask(state)} == {
                '(carrying)': 0, '(goal-achieved)': 0, '(max_int)': 20,
                '(maxx)': 4, '(maxy)': 4, '(minx)': 1, '(miny)': 1,
                '(poured plant1)': 0, '(total-time)': 0, '(total_loaded)': 0,
                '(total_poured)': 0, '(x agent1)': 1, '(x plant1)': 2,
                '(x tap1)': 4, '(y agent1)': 3, '(y plant1)': 2, '(y tap1)': 4
            }
