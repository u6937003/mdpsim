import mdpsim
import os

def main():
    my_path = os.path.dirname(os.path.abspath(__file__))
    domain_path = os.path.join(my_path, '..', 'examples', 'd.pddl')
    problem_path = os.path.join(my_path, '..', 'examples', 'p.pddl')

    mdpsim.parse_file(domain_path)
    mdpsim.parse_file(problem_path)

    print(f'Domains: {mdpsim.get_domains()}\n')
    
    for domain in mdpsim.get_domains().values():
        print(f'Domain: {domain.name}')
        print(f'  Types: {domain.types}')
        print(f'  Predicates: {domain.predicates}')
        print(f'  Lifted actions: {domain.lifted_actions}')
        print(f'  Functions: {domain.functions}')
        print(f'  Lifted comparison: {domain.lifted_comparisons}')
        print('')
    
    print(f'Problems: {mdpsim.get_problems()}\n')
    
    for problem in mdpsim.get_problems().values():
        print(f'Problem: {problem.name}')
        print(f'  Domain: {problem.domain}')
        print(f'  Propositions: {problem.propositions}')
        print(f'  Ground actions: {problem.ground_actions}')
        print(f'  Fluents: {problem.fluents}')
        print('')



if __name__ == '__main__':
    main()