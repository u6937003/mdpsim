(define (problem instance_6_bad)
  (:domain fn-counters)
  (:objects
    c0 c1 c2 c3 c4 c5 - counter
  )

  (:init
    (= (max_int) 8)
    (= (value c0) 0)
    (= (value c1) 0)
    (= (value c2) 0)
    (= (value c3) 0)
    (= (value c4) 0)
    (= (value c5) 0)
  )

  (:goal (and 
    (= (value c0) 0)
    (= (value c1) 1)
    (= (value c2) 2)
    (= (value c3) 3)
    (= (value c4) 4)
    (= (value c5) 5)
  ))
)
